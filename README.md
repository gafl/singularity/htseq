# htseq Singularity container
### Bionformatics package htseq<br>
HTSeq is a Python library to facilitate processing and analysis of data from high-throughput sequencing (HTS) experiments.<br>
htseq Version: 0.12.4<br>
[https://github.com/simon-anders/htseq]

Singularity container based on the recipe: Singularity.htseq_v0.12.4

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build htseq_v0.12.4.sif Singularity.htseq_v0.12.4`

### Get image help
`singularity run-help ./htseq_v0.12.4.sif`

#### Default runscript: STAR
#### Usage:
  `htseq_v0.12.4.sif --help`<br>
    or:<br>
  `singularity exec htseq_v0.12.4.sif htseq --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull htseq_v0.12.4.sif oras://registry.forgemia.inra.fr/gafl/singularity/htseq/htseq:latest`


